//
//  CVLoader.swift
//  CVApp
//
//  Created by Dariusz Zajac on 30/05/2020.
//  Copyright © 2020 Dariusz Zajac. All rights reserved.
//

import Foundation

protocol CVLoader: AnyObject {
    func load(from url: URL) throws -> CV
}

final class CVLoaderImpl: CVLoader {
    func load(from url: URL) throws -> CV {
        let data = try Data(contentsOf: url)
        let cv = try JSONDecoder().decode(CV.self, from: data)
        
        return cv
    }
}
