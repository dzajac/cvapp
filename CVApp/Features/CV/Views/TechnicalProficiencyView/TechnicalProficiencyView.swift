//
//  TechnicalProficiencyView.swift
//  CVApp
//
//  Created by Dariusz Zajac on 30/05/2020.
//  Copyright © 2020 Dariusz Zajac. All rights reserved.
//

import UIKit

class TechnicalProficiencyView: View<TechnicalProficiencyViewModel> {
    private enum Constant {
        static let stackViewSpacing: CGFloat = 8.0
    }

    private let stackView = UIStackView()
    
    override func setup() {
        super.setup()
        populateProficiencies()
    }
    
    override func setupView() {
        super.setupView()
        addSubview(stackView)
    }
    
    override func setupProperties() {
        super.setupProperties()
        setupStackViewProperties()
    }
    
    override func setupLayout() {
        super.setupLayout()
        setupStackViewLayout()
    }
}

private extension TechnicalProficiencyView {
    func setupStackViewProperties() {
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.spacing = Constant.stackViewSpacing
        stackView.axis = .vertical
    }
    
    func setupStackViewLayout() {
        NSLayoutConstraint.activate([
            stackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: trailingAnchor),
            stackView.topAnchor.constraint(equalTo: topAnchor),
            stackView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
    
    func populateProficiencies() {
        for proficiency in viewModel.proficiencies {
            let proficiencyComponent = ProficiencyComponent(
                with: ProficiencyComponentModel(title: proficiency.title,
                                                description: proficiency.description))
            stackView.addArrangedSubview(proficiencyComponent)
        }
    }
}
