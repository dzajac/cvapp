//
//  EducationView.swift
//  CVApp
//
//  Created by Dariusz Zajac on 30/05/2020.
//  Copyright © 2020 Dariusz Zajac. All rights reserved.
//

import UIKit

class EducationView: View<EducationViewModel> {
    private enum Constant {
        static let fontSize: CGFloat = 12.0
        static let verticalPadding: CGFloat = 8.0
    }

    private let programLabel = UILabel()
    private let timePeriodLabel = UILabel()
    private let schoolLabel = UILabel()
    
    override func setupView() {
        super.setupView()
        addSubview(programLabel)
        addSubview(timePeriodLabel)
        addSubview(schoolLabel)
    }
    
    override func setupProperties() {
        super.setupProperties()
        setupProgramLabelProperties()
        setupTimePeriodLabelProperties()
        setupSchoolLabelProperties()
    }
    
    override func setupLayout() {
        super.setupLayout()
        setupProgramLabelLayout()
        setupSchoolLabelLayout()
        setupTimePeriodLabelLayout()
    }
}

private extension EducationView {
    func setupProgramLabelProperties() {
        programLabel.numberOfLines = 0
        programLabel.font = .systemFont(ofSize: Constant.fontSize)
        programLabel.translatesAutoresizingMaskIntoConstraints = false
        
        programLabel.text = viewModel.program
    }
    
    func setupTimePeriodLabelProperties() {
        timePeriodLabel.font = .italicSystemFont(ofSize: Constant.fontSize)
        timePeriodLabel.translatesAutoresizingMaskIntoConstraints = false
        
        timePeriodLabel.text = viewModel.timePeriod
    }
    
    func setupSchoolLabelProperties() {
        schoolLabel.font = .systemFont(ofSize: Constant.fontSize)
        schoolLabel.translatesAutoresizingMaskIntoConstraints = false
        
        schoolLabel.text = viewModel.school
    }
    
    func setupProgramLabelLayout() {
        NSLayoutConstraint.activate([
            programLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            programLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            programLabel.topAnchor.constraint(equalTo: timePeriodLabel.bottomAnchor, constant: Constant.verticalPadding)
        ])
    }
    
    func setupSchoolLabelLayout() {
        NSLayoutConstraint.activate([
            schoolLabel.leadingAnchor.constraint(equalTo: programLabel.leadingAnchor),
            schoolLabel.topAnchor.constraint(equalTo: programLabel.bottomAnchor, constant: Constant.verticalPadding),
            schoolLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            schoolLabel.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
    
    func setupTimePeriodLabelLayout() {
        NSLayoutConstraint.activate([
            timePeriodLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            timePeriodLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            timePeriodLabel.topAnchor.constraint(equalTo: topAnchor),
        ])
    }
}
