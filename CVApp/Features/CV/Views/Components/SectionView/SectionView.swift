//
//  SectionView.swift
//  CVApp
//
//  Created by Dariusz Zajac on 30/05/2020.
//  Copyright © 2020 Dariusz Zajac. All rights reserved.
//

import UIKit

class SectionContainerView<ViewType: CVViewProtocol>: UIView {
    private let viewModel: SectionViewModel
    private let stackView = UIStackView()
    private let expandButton = UIButton()
    private let titleView: TitleView
    private let containerView: ViewType
    private var isCollapsed = false
    
    init(with viewModel: SectionViewModel, containerViewModel: ViewType.ModelType) {
        self.viewModel = viewModel
        self.containerView = ViewType.init(with: containerViewModel)
        self.titleView = TitleView(with: TitleViewModel(title: viewModel.sectionTitle))
        super.init(frame: .zero)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(with coder:) not implemented. Use init(with viewModel:containerViewModel")
    }
    
    @objc
    private func didTapExpandButton() {
        isCollapsed.toggle()
        let rotationAngle = isCollapsed ? 2*CGFloat.pi : -CGFloat.pi
        UIView.animate(withDuration: 0.2) {
            self.expandButton.transform = CGAffineTransform(rotationAngle: rotationAngle)
            (self.containerView as? UIView)?.isHidden = self.isCollapsed
            (self.containerView as? UIView)?.alpha = self.isCollapsed ? 0 : 1
        }
    }
}

private extension SectionContainerView {
    func setup() {
        setupView()
        setupProperties()
        setupLayout()

        addGestureRecognizers()
    }
    
    func setupView() {
        addSubview(stackView)
        addSubview(expandButton)
        stackView.addArrangedSubview(titleView)
        if let containerView = containerView as? UIView {
            stackView.addArrangedSubview(containerView)
        }
    }
    
    func setupProperties() {
        setupStackViewProperties()
        setupExpandButtonProperties()
    }
    
    func setupLayout() {
        setupStackViewLayout()
        setupExpandButtonLayout()
    }
    
    func setupStackViewProperties() {
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 4.0
    }
    
    func setupExpandButtonProperties() {
        expandButton.translatesAutoresizingMaskIntoConstraints = false
        expandButton.setTitle("", for: .normal)
        expandButton.setImage(UIImage(named:"expand"), for: .normal)
        expandButton.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
        expandButton.addTarget(self, action: #selector(didTapExpandButton), for: .touchUpInside)
    }
    
    func setupStackViewLayout() {
        NSLayoutConstraint.activate([
            stackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: trailingAnchor),
            stackView.topAnchor.constraint(equalTo: topAnchor),
            stackView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
    
    func setupExpandButtonLayout() {
        NSLayoutConstraint.activate([
            expandButton.widthAnchor.constraint(equalToConstant: 15.0),
            expandButton.heightAnchor.constraint(equalToConstant: 15.0),
            expandButton.trailingAnchor.constraint(equalTo: stackView.trailingAnchor),
            expandButton.topAnchor.constraint(equalTo: titleView.topAnchor)
        ])
    }

    func addGestureRecognizers() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(didTapExpandButton))
        titleView.addGestureRecognizer(tap)
    }
}
