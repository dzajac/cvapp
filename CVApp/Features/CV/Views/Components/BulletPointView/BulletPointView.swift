//
//  BulletPointView.swift
//  CVApp
//
//  Created by Dariusz Zajac on 30/05/2020.
//  Copyright © 2020 Dariusz Zajac. All rights reserved.
//

import UIKit

class BulletPointView: View<BulletPointViewModel> {
    private enum Constant {
        static let bulletPointSize: CGFloat = 10.0
        static let textTopPadding: CGFloat = 2.0
        static let textLeadingPadding: CGFloat = 8.0
    }
    private let bulletImageView = UIImageView()
    private let textLabel = UILabel()
    
    var font = UIFont.systemFont(ofSize: 12.0) {
        didSet {
            setupTextLabelProperties()
        }
    }
    
    override func setupView() {
        super.setupView()
        addSubview(bulletImageView)
        addSubview(textLabel)
    }
    
    override func setupProperties() {
        super.setupProperties()
        setupBulletImageProperties()
        setupTextLabelProperties()
    }
    
    override func setupLayout() {
        super.setupLayout()
        setupBulletPointLayout()
        setupTextLabelLayout()
    }
}

private extension BulletPointView {
    func setupBulletImageProperties() {
        bulletImageView.translatesAutoresizingMaskIntoConstraints = false
        bulletImageView.contentMode = .scaleAspectFill
        bulletImageView.image = UIImage(named: "bullet")
    }
    
    func setupTextLabelProperties() {
        textLabel.numberOfLines = 0
        textLabel.translatesAutoresizingMaskIntoConstraints = false
        textLabel.font = .systemFont(ofSize: 12.0)
        textLabel.text = viewModel.text
    }
    
    func setupBulletPointLayout() {
        NSLayoutConstraint.activate([
            bulletImageView.widthAnchor.constraint(equalToConstant: Constant.bulletPointSize).withPriority(999),
            bulletImageView.heightAnchor.constraint(equalToConstant: Constant.bulletPointSize).withPriority(999),
            bulletImageView.leadingAnchor.constraint(equalTo: leadingAnchor),
            bulletImageView.topAnchor.constraint(equalTo: topAnchor),
            bulletImageView.bottomAnchor.constraint(lessThanOrEqualTo: bottomAnchor)
        ])
    }
    
    func setupTextLabelLayout() {
        NSLayoutConstraint.activate([
            textLabel.topAnchor.constraint(equalTo: bulletImageView.topAnchor,
                                           constant: -Constant.textTopPadding).withPriority(999),
            textLabel.leadingAnchor.constraint(equalTo: bulletImageView.trailingAnchor,
                                               constant: Constant.textLeadingPadding).withPriority(999),
            textLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            textLabel.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
}
