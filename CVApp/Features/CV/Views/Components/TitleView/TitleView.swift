//
//  TitleView.swift
//  CVApp
//
//  Created by Dariusz Zajac on 30/05/2020.
//  Copyright © 2020 Dariusz Zajac. All rights reserved.
//

import UIKit

class TitleView: View<TitleViewModel> {
    private enum Constant {
        static let titleFontSize: CGFloat = 16.0
        static let dividerTopPadding: CGFloat = 2.0
        static let dividerBottomPadding: CGFloat = 4.0
        static let dividerHeight: CGFloat = 2.0
    }
    private var titleLabel = UILabel()
    private let divider = UIView()
    
    override func setupView() {
        super.setupView()
        addSubview(titleLabel)
        addSubview(divider)
    }
    
    override func setupProperties() {
        super.setupProperties()
        setupTitleLabelProperties()
        setupDividerProperties()
    }
    
    override func setupLayout() {
        super.setupLayout()
        setupTitleLabelLayout()
        setupDividerLayout()
    }
}

private extension TitleView {
    func setupTitleLabelProperties() {
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.font = .boldSystemFont(ofSize: Constant.titleFontSize)
        
        titleLabel.text = viewModel.title.uppercased()
    }
    
    func setupDividerProperties() {
        divider.backgroundColor = .black
        divider.translatesAutoresizingMaskIntoConstraints = false
    }
    
    func setupTitleLabelLayout() {
        NSLayoutConstraint.activate([
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            titleLabel.trailingAnchor.constraint(lessThanOrEqualTo: trailingAnchor),
            titleLabel.topAnchor.constraint(equalTo: topAnchor)
        ])
    }
    
    func setupDividerLayout() {
        NSLayoutConstraint.activate([
            divider.leadingAnchor.constraint(equalTo: leadingAnchor),
            divider.trailingAnchor.constraint(equalTo: trailingAnchor),
            divider.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: Constant.dividerTopPadding),
            divider.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -Constant.dividerBottomPadding),
            divider.heightAnchor.constraint(equalToConstant: Constant.dividerHeight)
        ])
    }
}
