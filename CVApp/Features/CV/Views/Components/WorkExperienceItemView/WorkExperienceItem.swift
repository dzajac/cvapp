//
//  WorkExperienceItem.swift
//  CVApp
//
//  Created by Dariusz Zajac on 31/05/2020.
//  Copyright © 2020 Dariusz Zajac. All rights reserved.
//

import UIKit

class WorkExperienceItemView: View<WorkExperienceItemViewModel> {
    private enum Constant {
        static let fontSize: CGFloat = 12.0
        static let labelPadding: CGFloat = 4.0
        static let stackViewPadding: CGFloat = 16.0
    }

    private let stackView = UIStackView()
    private let roleLabel = UILabel()
    private let companyLabel = UILabel()
    private let timePeriodLabel = UILabel()
    
    override func setup() {
        super.setup()
        populateResponsibilities()
    }
    
    override func setupView() {
        super.setupView()
        addSubview(timePeriodLabel)
        addSubview(roleLabel)
        addSubview(companyLabel)
        addSubview(stackView)
    }
    
    override func setupProperties() {
        super.setupProperties()
        setupStackViewProperties()
        setupRoleLabelProperties()
        setupCompanyLabelProperties()
        setupTimePeriodLabelProperties()
    }
    
    override func setupLayout() {
        super.setupLayout()
        setupStackViewLayout()
        setupRoleLabelLayout()
        setupCompanyLabelLayout()
        setupTimePeriodLabelLayout()
    }
}

private extension WorkExperienceItemView {
    func setupTimePeriodLabelProperties() {
        timePeriodLabel.translatesAutoresizingMaskIntoConstraints = false
        timePeriodLabel.font = .italicSystemFont(ofSize: Constant.fontSize)
        timePeriodLabel.text = viewModel.timePeriod
    }
    
    func setupRoleLabelProperties() {
        roleLabel.translatesAutoresizingMaskIntoConstraints = false
        roleLabel.numberOfLines = 0
        roleLabel.font = .boldSystemFont(ofSize: Constant.fontSize)
        roleLabel.text = viewModel.role
    }
    
    func setupCompanyLabelProperties() {
        companyLabel.translatesAutoresizingMaskIntoConstraints = false
        companyLabel.numberOfLines = 0
        companyLabel.font = .italicSystemFont(ofSize: Constant.fontSize)
        companyLabel.text = viewModel.company
    }
    
    func setupStackViewProperties() {
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 8.0
    }
    
    func setupTimePeriodLabelLayout() {
        NSLayoutConstraint.activate([
            timePeriodLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            timePeriodLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            timePeriodLabel.topAnchor.constraint(equalTo: topAnchor)
        ])
    }
    
    func setupRoleLabelLayout() {
        NSLayoutConstraint.activate([
            roleLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            roleLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            roleLabel.topAnchor.constraint(equalTo: timePeriodLabel.bottomAnchor, constant: Constant.labelPadding)
        ])
    }
    
    func setupCompanyLabelLayout() {
        NSLayoutConstraint.activate([
            companyLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            companyLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            companyLabel.topAnchor.constraint(equalTo: roleLabel.bottomAnchor, constant: Constant.labelPadding)
        ])
    }
    
    func setupStackViewLayout() {
        NSLayoutConstraint.activate([
            stackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: trailingAnchor),
            stackView.topAnchor.constraint(equalTo: companyLabel.bottomAnchor, constant: Constant.stackViewPadding),
            stackView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
    
    func populateResponsibilities() {
        let responsibilitiesViews = viewModel.responsibilities
            .map { BulletPointView(with: BulletPointViewModel(text: $0)) }
        
        responsibilitiesViews.forEach {
            stackView.addArrangedSubview($0)
        }
    }
}
