//
//  ProficiencyComponent.swift
//  CVApp
//
//  Created by Dariusz Zajac on 30/05/2020.
//  Copyright © 2020 Dariusz Zajac. All rights reserved.
//

import UIKit

class ProficiencyComponent: View<ProficiencyComponentModel> {
    private enum Constant {
        static let titleFontSize: CGFloat = 16.0
        static let descriptionFontSize: CGFloat = 12.0
        static let descriptionTopPadding: CGFloat = 4.0
    }

    private let titleLabel = UILabel()
    private let descriptionLabel = UILabel()
    
    override func setupView() {
        super.setupView()
        addSubview(titleLabel)
        addSubview(descriptionLabel)
    }
    
    override func setupProperties() {
        super.setupProperties()
        setupTitleLabelProperties()
        setupDescriptionLabelProperties()
    }
    
    override func setupLayout() {
        super.setupLayout()
        setupTitleLabelLayout()
        setupDescriptionLabelLayout()
    }
}

private extension ProficiencyComponent {
    func setupTitleLabelProperties() {
        titleLabel.font = .boldSystemFont(ofSize: Constant.titleFontSize)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.text = viewModel.title
    }
    
    func setupDescriptionLabelProperties() {
        descriptionLabel.numberOfLines = 0
        descriptionLabel.font = .systemFont(ofSize: Constant.descriptionFontSize)
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        descriptionLabel.text = viewModel.description
    }
    
    func setupTitleLabelLayout() {
        NSLayoutConstraint.activate([
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            titleLabel.topAnchor.constraint(equalTo: topAnchor)
        ])
    }
    
    func setupDescriptionLabelLayout() {
        NSLayoutConstraint.activate([
            descriptionLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            descriptionLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            descriptionLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: Constant.descriptionTopPadding),
            descriptionLabel.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
}
