//
//  WorkExperienceViewModel.swift
//  CVApp
//
//  Created by Dariusz Zajac on 31/05/2020.
//  Copyright © 2020 Dariusz Zajac. All rights reserved.
//

import Foundation

struct WorkExperienceViewModel: ViewModel {
    var workExperienceItems: [WorkExperienceItem]?
}
