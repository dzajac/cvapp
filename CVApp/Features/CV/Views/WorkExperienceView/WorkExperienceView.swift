//
//  WorkExperienceView.swift
//  CVApp
//
//  Created by Dariusz Zajac on 31/05/2020.
//  Copyright © 2020 Dariusz Zajac. All rights reserved.
//

import UIKit

class WorkExperienceView: View<WorkExperienceViewModel> {
    private let stackView = UIStackView()
    
    override func setup() {
        super.setup()
        populateWorkExperience()
    }
    
    override func setupView() {
        super.setupView()
        addSubview(stackView)
    }
    
    override func setupProperties() {
        super.setupProperties()
        setupStackViewProperties()
    }
    
    override func setupLayout() {
        super.setupLayout()
        setupStackViewLayout()
    }
}

private extension WorkExperienceView {
    func setupStackViewProperties() {
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 16.0
    }
    
    func setupStackViewLayout() {
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: topAnchor),
            stackView.bottomAnchor.constraint(equalTo: bottomAnchor),
            stackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: trailingAnchor)
        ])
    }
    
    func populateWorkExperience() {
        guard let workExperienceItems = viewModel.workExperienceItems else {
            return
        }
        
        let workExperienceItemViews = workExperienceItems
            .map { WorkExperienceItemView(with: WorkExperienceItemViewModel(
                role: $0.role,
                company: $0.company,
                timePeriod: $0.timePeriod,
                responsibilities: $0.responsibilities
            ))}
        
        workExperienceItemViews.forEach {
            stackView.addArrangedSubview($0)
        }
    }
}
