//
//  View.swift
//  CVApp
//
//  Created by Dariusz Zajac on 30/05/2020.
//  Copyright © 2020 Dariusz Zajac. All rights reserved.
//

import UIKit

protocol CVViewProtocol {
    associatedtype ModelType: ViewModel
    init(with viewModel: ModelType)
}

class View<ModelType: ViewModel>: UIView, CVViewProtocol {
    var viewModel: ModelType

    required init(with viewModel: ModelType) {
        self.viewModel = viewModel
        super.init(frame: .zero)
        clipsToBounds = true
        setup()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(with coder:) not available. Use init(with viewModel:).")
    }
    
    func setup() {
        setupView()
        setupProperties()
        setupLayout()
    }

    func setupView() {}
    func setupProperties() {}
    func setupLayout() {}
}
