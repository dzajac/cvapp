//
//  CVView.swift
//  CVApp
//
//  Created by Dariusz Zajac on 30/05/2020.
//  Copyright © 2020 Dariusz Zajac. All rights reserved.
//

import UIKit

class CVView: View<CVViewModel> {
    private enum Constant {
        static let stackViewVerticalPadding: CGFloat = 8.0
        static let stackViewHorizontalPadding: CGFloat = 20.0
    }
    private let scrollView = UIScrollView()
    private let contentView = UIView()
    private let stackView = UIStackView()
    private let contactInfoView: ContactInfoView
    private let divider = UIView()
    private let objectiveSectionView: SectionContainerView<ObjectiveView>
    private let technicalProficiencyView: SectionContainerView<TechnicalProficiencyView>
    private let educationView: SectionContainerView<EducationView>
    private let skillsView: SectionContainerView<SkillsView>
    private let workExperienceView: SectionContainerView<WorkExperienceView>
    
    required init(with viewModel: CVViewModel) {
        self.contactInfoView = ContactInfoView(with: ContactInfoViewModel(contactInfo: viewModel.cv.contactInfo))
        self.objectiveSectionView = SectionContainerView(
            with: SectionViewModel(sectionTitle: "objective".localized),
            containerViewModel: ObjectiveViewModel(objective: viewModel.cv.objective))
        self.technicalProficiencyView = SectionContainerView(
            with: SectionViewModel(sectionTitle: "technical proficiency".localized),
            containerViewModel: TechnicalProficiencyViewModel(proficiencies: viewModel.cv.technicalProficiency))
        self.skillsView = SectionContainerView(
            with: SectionViewModel(sectionTitle: "skills summary".localized),
            containerViewModel: SkillsViewModel(skills: viewModel.cv.skills ?? [])
        )
        self.educationView = SectionContainerView(
            with: SectionViewModel(sectionTitle: "education".localized),
            containerViewModel: EducationViewModel(program: viewModel.cv.education.program,
                                                   timePeriod: viewModel.cv.education.timePeriod,
                                                   school: viewModel.cv.education.school))
        self.workExperienceView = SectionContainerView(
            with: SectionViewModel(sectionTitle: "work experience".localized),
            containerViewModel: WorkExperienceViewModel(workExperienceItems: viewModel.cv.workExperience)
        )
        super.init(with: viewModel)
    }
    
    override func setupView() {
        super.setupView()
        addSubview(scrollView)
        scrollView.addSubview(contentView)
        contentView.addSubview(divider)
        contentView.addSubview(stackView)
        stackView.addArrangedSubview(contactInfoView)
        stackView.addArrangedSubview(divider)
        stackView.addArrangedSubview(objectiveSectionView)
        stackView.addArrangedSubview(technicalProficiencyView)
        stackView.addArrangedSubview(skillsView)
        stackView.addArrangedSubview(educationView)
        stackView.addArrangedSubview(workExperienceView)
    }
    
    override func setupProperties() {
        super.setupProperties()
        setupScrollViewProperties()
        setupContentViewProperties()
        setupDividerProperties()
        setupStackViewProperties()
        setupContactInfoViewProperties()
    }
    
    override func setupLayout() {
        super.setupLayout()
        setupScrollViewLayout()
        setupContentViewLayout()
        setupStackViewLayout()
    }
}

private extension CVView {
    func setupScrollViewProperties() {
        scrollView.translatesAutoresizingMaskIntoConstraints = false
    }
    
    func setupStackViewProperties() {
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 16.0
    }
    
    func setupContactInfoViewProperties() {
        contactInfoView.translatesAutoresizingMaskIntoConstraints = false
    }
    
    func setupContentViewProperties() {
        contentView.translatesAutoresizingMaskIntoConstraints = false
    }
    
    func setupDividerProperties() {
        divider.translatesAutoresizingMaskIntoConstraints = false
        divider.backgroundColor = .black
        divider.heightAnchor.constraint(equalToConstant: 20.0).isActive = true
    }
    
    func setupContentViewLayout() {
        NSLayoutConstraint.activate([
            contentView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            contentView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            contentView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            contentView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor)
        ])
    }
    
    func setupScrollViewLayout() {
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: layoutMarginsGuide.topAnchor),
            scrollView.bottomAnchor.constraint(equalTo: layoutMarginsGuide.bottomAnchor),
            scrollView.leadingAnchor.constraint(equalTo: leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: trailingAnchor),
            scrollView.widthAnchor.constraint(equalTo: contentView.widthAnchor)
        ])
    }
    
    func setupStackViewLayout() {
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: Constant.stackViewVerticalPadding),
            stackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -Constant.stackViewVerticalPadding),
            stackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: Constant.stackViewHorizontalPadding),
            stackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -Constant.stackViewHorizontalPadding)
        ])
    }
}
