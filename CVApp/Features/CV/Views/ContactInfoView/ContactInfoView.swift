//
//  ContactInfoView.swift
//  CVApp
//
//  Created by Dariusz Zajac on 30/05/2020.
//  Copyright © 2020 Dariusz Zajac. All rights reserved.
//

import UIKit

class ContactInfoView: View<ContactInfoViewModel> {
    private enum Constant {
        static let primaryFontSize: CGFloat = 18.0
        static let secondaryFontSize: CGFloat = 12.0
        static let stackViewSpacing: CGFloat = 4.0
    }

    private var leftStackView = UIStackView()
    private var rightStackView = UIStackView()
    
    private var nameLabel = UILabel()
    private var emailLabel = UILabel()
    private var phoneLabel = UILabel()
    private var streetLabel = UILabel()
    private var postCodeLabel = UILabel()
    private var cityCountryLabel = UILabel()
    
    override func setupView() {
        super.setupView()
        addSubview(leftStackView)
        addSubview(rightStackView)
        
        leftStackView.addArrangedSubview(nameLabel)
        leftStackView.addArrangedSubview(emailLabel)
        leftStackView.addArrangedSubview(phoneLabel)
        
        rightStackView.addArrangedSubview(streetLabel)
        rightStackView.addArrangedSubview(postCodeLabel)
        rightStackView.addArrangedSubview(cityCountryLabel)
    }
    
    override func setupProperties() {
        super.setupProperties()
        
        setupLeftStackViewProperties()
        setupRightStackViewProperties()
        setupNameLabelProperties()
        setupEmailLabelProperies()
        setupPhoneLabelProperties()
        setupStreetLabelProperties()
        setupPostCodeLabelProperties()
        setupCityAndCountryLabelProperties()
    }
    
    override func setupLayout() {
        super.setupLayout()
        
        setupLeftStackViewLayout()
        setupRightStackViewLayout()
    }
    
    @objc private func didTapEmail() {
        EmailHelper.send(to: viewModel.contactInfo.email)
    }
    
    @objc private func didTapPhone() {
        guard let phoneNumber = viewModel.contactInfo.phone else {
            return
        }
        CallHelper.call(number: phoneNumber)
    }
}

private extension ContactInfoView {
    func setupLeftStackViewProperties() {
        leftStackView.translatesAutoresizingMaskIntoConstraints = false
        leftStackView.axis = .vertical
        leftStackView.alignment = .leading
        leftStackView.spacing = Constant.stackViewSpacing
    }
    
    func setupRightStackViewProperties() {
        rightStackView.translatesAutoresizingMaskIntoConstraints = false
        rightStackView.axis = .vertical
        rightStackView.alignment = .trailing
        rightStackView.spacing = Constant.stackViewSpacing
    }
    
    func setupNameLabelProperties() {
        nameLabel.font = .boldSystemFont(ofSize: Constant.primaryFontSize)
        nameLabel.text = viewModel.contactInfo.fullName
    }
    
    func setupEmailLabelProperies() {
        emailLabel.font = .systemFont(ofSize: Constant.secondaryFontSize)
        emailLabel.isUserInteractionEnabled = true
        let attributedString = NSAttributedString(string: viewModel.contactInfo.email,
                                        attributes: Constants.linkAttributes)
        emailLabel.attributedText = attributedString
        emailLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapEmail)))
    }
    
    func setupPhoneLabelProperties() {
        phoneLabel.font = .systemFont(ofSize: Constant.secondaryFontSize)
        phoneLabel.isUserInteractionEnabled = true
        if let phoneNumber = viewModel.contactInfo.phone {
            let phoneNumberString = String(format: "telephone".localized, phoneNumber)
            let attributedString = NSAttributedString(string: phoneNumberString, attributes: Constants.linkAttributes)
            phoneLabel.attributedText = attributedString
            phoneLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapPhone)))
        }
    }
    
    func setupStreetLabelProperties() {
        streetLabel.font = .systemFont(ofSize: Constant.secondaryFontSize)
        streetLabel.text = viewModel.contactInfo.street
    }
    
    func setupPostCodeLabelProperties() {
        postCodeLabel.font = .systemFont(ofSize: Constant.secondaryFontSize)
        postCodeLabel.text = viewModel.contactInfo.postCode
    }
    
    func setupCityAndCountryLabelProperties() {
        cityCountryLabel.font = .systemFont(ofSize: Constant.secondaryFontSize)
        cityCountryLabel.text = "\(viewModel.contactInfo.city), \(viewModel.contactInfo.country)"
    }
    
    func setupLeftStackViewLayout() {
        NSLayoutConstraint.activate([
            leftStackView.topAnchor.constraint(equalTo: topAnchor),
            leftStackView.bottomAnchor.constraint(lessThanOrEqualTo: bottomAnchor),
            leftStackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            leftStackView.trailingAnchor.constraint(equalTo: centerXAnchor)
            ])
    }
    
    func setupRightStackViewLayout() {
        NSLayoutConstraint.activate([
            rightStackView.topAnchor.constraint(equalTo: leftStackView.topAnchor),
            rightStackView.bottomAnchor.constraint(equalTo: bottomAnchor),
            rightStackView.leadingAnchor.constraint(equalTo: centerXAnchor),
            rightStackView.trailingAnchor.constraint(equalTo: trailingAnchor)
            ])
    }
}
