//
//  SkillsView.swift
//  CVApp
//
//  Created by Dariusz Zajac on 30/05/2020.
//  Copyright © 2020 Dariusz Zajac. All rights reserved.
//

import UIKit

class SkillsView: View<SkillsViewModel> {
    private enum Constant {
        static let stackViewSpacing: CGFloat = 8.0
    }
    private let stackView = UIStackView()
    
    override func setup() {
        super.setup()
        populateSkills()
    }
    
    override func setupView() {
        super.setupView()
        addSubview(stackView)
    }
    
    override func setupProperties() {
        super.setupProperties()
        setupStackViewProperties()
    }
    
    override func setupLayout() {
        super.setupLayout()
        setupStackViewLayout()
    }
}

private extension SkillsView {
    func setupStackViewProperties() {
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = Constant.stackViewSpacing
    }
    
    func setupStackViewLayout() {
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: topAnchor),
            stackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: trailingAnchor),
            stackView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
    
    func populateSkills() {
        let skillViews = viewModel.skills.map { BulletPointView(with: BulletPointViewModel(text: $0)) }
        
        skillViews.forEach {
            stackView.addArrangedSubview($0)
        }
    }
}
