//
//  SkillsViewModel.swift
//  CVApp
//
//  Created by Dariusz Zajac on 30/05/2020.
//  Copyright © 2020 Dariusz Zajac. All rights reserved.
//

import Foundation

struct SkillsViewModel: ViewModel {
    var skills: [String]
}
