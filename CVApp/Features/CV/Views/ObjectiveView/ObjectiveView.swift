//
//  ObjectiveView.swift
//  CVApp
//
//  Created by Dariusz Zajac on 30/05/2020.
//  Copyright © 2020 Dariusz Zajac. All rights reserved.
//

import UIKit

class ObjectiveView: View<ObjectiveViewModel> {
    private enum Constant {
        static let fontSize: CGFloat = 12.0
    }
    private let objectiveLabel = UILabel()
    
    override func setupView() {
        super.setupView()
        addSubview(objectiveLabel)
    }
    
    override func setupProperties() {
        super.setupProperties()
        setupObjectiveLabelProperties()
    }
    
    override func setupLayout() {
        super.setupLayout()
        setupObjectiveLabelLayout()
    }
}

private extension ObjectiveView {
    func setupObjectiveLabelProperties() {
        objectiveLabel.translatesAutoresizingMaskIntoConstraints = false
        objectiveLabel.numberOfLines = 0
        objectiveLabel.font = .systemFont(ofSize: Constant.fontSize)
        objectiveLabel.text = viewModel.objective
    }
    
    func setupObjectiveLabelLayout() {
        NSLayoutConstraint.activate([
            objectiveLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            objectiveLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            objectiveLabel.topAnchor.constraint(equalTo: topAnchor),
            objectiveLabel.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
}
