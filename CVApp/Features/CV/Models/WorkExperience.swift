//
//  WorkExperience.swift
//  CVApp
//
//  Created by Dariusz Zajac on 30/05/2020.
//  Copyright © 2020 Dariusz Zajac. All rights reserved.
//

import Foundation

struct WorkExperienceItem: Codable {
    var role: String
    var company: String
    var timePeriod: String
    var responsibilities: [String]
}
