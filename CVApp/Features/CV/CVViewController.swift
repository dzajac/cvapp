//
//  CVViewController.swift
//  CVApp
//
//  Created by Dariusz Zajac on 30/05/2020.
//  Copyright © 2020 Dariusz Zajac. All rights reserved.
//

import UIKit

class CVViewController: UIViewController {
    private let url: URL?
    private let cvLoader: CVLoader

    init(with url: URL?, cvLoader: CVLoader) {
        self.url = url
        self.cvLoader = cvLoader
        super.init(nibName: nil, bundle: nil)
        title = "cv".localized
        self.view.backgroundColor = .white
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented. Use init(with viewModel:)")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let cvView = loadCV() {
            addCVSubview(cvView)
        }
    }
}

private extension CVViewController {
    func loadCV() -> UIView? {
        guard let url = url else {
            showErrorAlert(message: "invalid url".localized)
            return nil
        }
        
        do {
            let cv = try cvLoader.load(from: url)
            let cvView = CVView(with: CVViewModel(cv: cv))
            return cvView
        } catch {
            showErrorAlert(message: "error loading cv".localized)
        }
        return nil
    }
    
    func showErrorAlert(message: String) {
        let alert = UIAlertController(title: "error".localized,
                                      message: message,
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "ok".localized, style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    func addCVSubview(_ subview: UIView) {
        subview.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(subview)
        NSLayoutConstraint.activate([
            subview.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            subview.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            subview.topAnchor.constraint(equalTo: view.topAnchor),
            subview.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
}
