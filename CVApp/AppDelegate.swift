//
//  AppDelegate.swift
//  CVApp
//
//  Created by Dariusz Zajac on 30/05/2020.
//  Copyright © 2020 Dariusz Zajac. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        let window = UIWindow()
        
        let url = Bundle.main.url(forResource: "cv", withExtension: "json")
        let cvLoader = CVLoaderImpl()
        let cvViewController = CVViewController(with: url, cvLoader: cvLoader)
        let navController = UINavigationController(rootViewController: cvViewController)
        window.rootViewController = navController

        window.makeKeyAndVisible()
        self.window = window
        return true
    }
    
}

