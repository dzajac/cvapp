//
//  Constants.swift
//  CVApp
//
//  Created by Dariusz Zajac on 31/05/2020.
//  Copyright © 2020 Dariusz Zajac. All rights reserved.
//

import Foundation
import UIKit

final class Constants {
    static let linkAttributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.underlineColor: UIColor.blue,
                                                                NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue,
                                                                NSAttributedString.Key.foregroundColor: UIColor.blue]
}
