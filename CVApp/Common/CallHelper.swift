//
//  CallHelper.swift
//  CVApp
//
//  Created by Dariusz Zajac on 31/05/2020.
//  Copyright © 2020 Dariusz Zajac. All rights reserved.
//

import UIKit

class CallHelper {
    static func call(number: String) {
        let phoneNumber = ((number as NSString).replacingOccurrences(of: " ", with: "") as String)
        if let url = URL(string: "tel://\(phoneNumber)") {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
}
